package edu.towson.cosc431.henrichs.tipcalculator_henrichs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

public class TipCalculatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_calculator);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){


                EditText editText=findViewById(R.id.editText);
                RadioGroup radioGroup=findViewById(R.id.radioGroup);
                RadioButton rb1=findViewById(R.id.radioButton);
                RadioButton rb2=findViewById(R.id.radioButton2);
                RadioButton rb3=findViewById(R.id.radioButton3);
                TextView textView=findViewById(R.id.textView7);

                DecimalFormat df=new DecimalFormat("#.##");
                double tipPercentage;
                String check=editText.getText().toString();
                double checkAmount=Double.parseDouble(check);

                if(rb1.getId()==radioGroup.getCheckedRadioButtonId()){
                    tipPercentage=.10;
                }else if(rb2.getId()==radioGroup.getCheckedRadioButtonId()){
                    tipPercentage=.20;
                }else{
                    tipPercentage=.30;
                }

                double tip=checkAmount*tipPercentage;
                double total=checkAmount+tip;

                String msg="Your calculated tip is $"+df.format(tip).toString()+" and your total is $"+df.format(total).toString();

                textView.setText(msg);
            }
        });
    }
}
